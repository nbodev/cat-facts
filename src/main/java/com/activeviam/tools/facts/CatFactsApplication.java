package com.activeviam.tools.facts;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.AllArgsConstructor;
import lombok.Data;

@SpringBootApplication
public class CatFactsApplication {
	private static final Logger log = LoggerFactory.getLogger(CatFactsApplication.class);
	private final List<CatFact> catFactsList = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(CatFactsApplication.class, args);
	}

	// loader
	@Bean
	void dataLoader() {
		final var fileName = "cat_facts.txt";

		String line;

		try (final var is = getClass().getClassLoader().getResourceAsStream(fileName);
				final var isr = new InputStreamReader(is);
				final var br = new BufferedReader(isr);) {

			while ((line = br.readLine()) != null) {
				final var parts = line.split("=");
				catFactsList.add(new CatFact(parts[0], Integer.valueOf(parts[1])));
			}
			log.info("Loaded [{}] entries from the file [{}].", catFactsList.size(), fileName);
		} catch (final Exception e) {
			log.error("Issue while filling list from the file [{}].", fileName, e);
		}
	}

	// model
	@Data
	@AllArgsConstructor
	public class CatFact {
		private String fact;
		private int length;
	}

	// controller
	@RestController
	public class ObservabilityController {

		@GetMapping("/fact")
		public CatFact getCatFact() {
			return Optional
					.ofNullable(catFactsList.get(ThreadLocalRandom.current().nextInt(catFactsList.size())))
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No fact available for now."));
		}

	}

}
