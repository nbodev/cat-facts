	
	                      /^--^\     /^--^\     /^--^\
	                      \____/     \____/     \____/
	                     /      \   /      \   /      \
	                    |        | |        | |        |
	                     \__  __/   \__  __/   \__  __/
	|^|^|^|^|^|^|^|^|^|^|^|^\ \^|^|^|^/ /^|^|^|^|^\ \^|^|^|^|^|^|^|^|^|^|^|^|
	| | | | | | | | | | | | |\ \| | |/ /| | | | | | \ \ | | | | | | | | | | |
	########################/ /######\ \###########/ /#######################
	| | | | | | | | | | | | \/| | | | \/| | | | | |\/ | | | | | | | | | | | |
	|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|
											

### Introduction

A simple SpringBoot 3 project with java 17 replicating the behavior of `https://catfact.ninja/fact` that you can run locally.<br>


### The application

Run `mvn clean package` in order to build the project.<br>
Either you start the application with `java -jar ./target/<jar name>` outside of your IDE or run the class `CatFactsApplication`.<br>


### The URLs

Hit the end point (use postman / curl / your browser) `http://localhost:9999/fact`, you will get a random fact about the cats and the length of this fact, below an example:

```
{
  "fact": "The heaviest cat on record is Himmy, a Tabby from Queensland, Australia. He weighed nearly 47 pounds (21 kg). He died at the age of 10.",
  "length": 135
}
```
Swagger is also enabled: `http://localhost:9999/swagger-ui/index.html`.<br>
Default port is `9999`, you can change it in `application.yml` or use this jvm arg in order to override it `-Dserver.port=<my desired port number>`.

